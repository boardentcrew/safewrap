# safeWrap #

safeWrap is used to add additional privatization of objects properties.

## Restricted Enviroments ##

safeWrap was invented to ensure security in restricted enviroments. Scope and context can be controlled using nodejs native VM module. safeWrap can be used along side to ensure secure and private variables scopes.

### Why use safeWrap? ###

* Help futher secure NPMs
* Create restricted npms
* Security


```
#!nodejs

var safeWrap = require('safeWrap');
var redisClient = safeWrap(require('redis').createClient(),[
        'set','hset','get','hget'
    ]);
// redisClient now a restricted redis client object, with only 4 exposed methods, all hidden behind private functions.

```