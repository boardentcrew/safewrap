var safeWrap = require('./index.js');

var testObj = {
	name:'Dan',
	talk:function(words) {
		console.log('From',testObj.name,':',words);
	},
	password:'thatsasecret'
}

var newObj = safeWrap(testObj,['name','talk']);

for(i in newObj) {
	console.log(i,newObj[i]);
}



newObj.talk('Hello World!','How are you','today?');
