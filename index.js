var objs=[]; //Hidden array of referance objects

function makeid() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for( var i=0; i < 20; i++ ) text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
}

function wrap(obj,allowable) {
	var out = Object.create(null);
	out._id = makeid();
	objs[out._id] = obj;
	for (propIndex in Object.getOwnPropertyNames(obj)) {
		var propName = Object.getOwnPropertyNames(obj)[propIndex-1];		
		for (allowed in allowable) {
			console.log(allowable[allowed]);
			if (allowable[allowed] == propName) { //Allowed public property
				switch(typeof(objs[out._id][propName])) {
					case 'function' :
						console.log(propName,typeof(objs[out._id][propName]));
						out[propName] = function() {
							return objs[out._id][propName].apply(objs[out._id][propName],arguments);
						}
						break;
					case 'obj':
						Object.defineProperty(out, propName, {
							configurable: false,
							enumerable: true,
							value: objs[out._id][propName]
						});
						break;
					case 'string':
					case 'boolean':
					case 'number' : 
						Object.defineProperty(out, propName, {
							configurable: false,
							enumerable: true,
							value: objs[out._id][propName]
						});
						break;
				}
			}
		}
	}
	return out;
}
module.exports = wrap;
